#!/bin/bash

# Python Environment for Vim and Ubuntu
## For Projecs and Learning
### Copy this file to your home directory before editing

print_usage () {
	#### $1 : script name
	echo "usage: $1 project_name git_url"
}
install_packages () {
	### Install required packages
	sudo apt update -y && sudo apt install git vim virtualenv -y
	if [ $? -ne 0 ]; then
		>&2 echo "Unable to install required Packages"
		exit 2
	fi
}

make_env () {
	### Create virtualenv
	#### $1 : project name
	if [ ! -v $HOME ]; then
		if [ -d "$HOME/$1" ]; then 
			>&2 echo "project directory name exists!"
			exit 4
		else
			>&2 virtualenv "$HOME/$1"
			if [ $? -ne 0 ]; then
				>&2 echo "please run $0 --install to install virtualenv and git packages."
				exit 3
			fi
		fi
	else
		>&2 echo "no HOME directory set for user"
	fi
	### Enter into the new environment
	if [ -d "$HOME/$1" ]; then
		cd "$HOME/$1"
	else
		print_usage
		exit 1
	fi

}

make_repo () {
	### Start a new project
	#### $1 : project name
	#### $2 : git URL
	mkdir $1 && cd $1
	touch README.md
	git init
	git checkout -b main
	git add README.md
	git commit -m "first commit"
	git remote add origin $2
	git push -u origin main
	if [ $? -ne 0 ]; then
		>2& echo "Error pushing repo to $2."
	else
		exit 0
	fi
}
clone_repo () {
	### Download project
	#### $1 : git URL
	git clone $1
}

### Install packages if flagged
if [ "$1" == "--install" ]; then 
	install_packages
	exit 0
### Input validation must have args	
elif [ $# -lt 2 ]; then
	>&2 print_usage
	exit 1
### Run make_repo instead of clone_repo if flagged
elif [ "$3" == "--new" ]; then
	make_env $1
	make_repo $1 $2
	exit 0
### Standard usage
elif [ $# -eq 2 ]; then
	make_env $1
	clone_repo $2
	exit 0
fi

